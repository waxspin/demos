# DCT and tests

To build and run this example project, you will need CMake, and the GCC toolchain (or Clang/LLVM if you're on a Mac).
Once you've cloned it and cd'd into the repo directory, type
``````
mkdir build && cd build && cmake ../
``````
to configure the build. Then, type ```make``` to build everything. One can see that there is a static library, libdct.a
 that is built, which contains the actual DCT itself, along with two exectuables, dcttest and computedct.
 
The first, computedct can be run as:
`````` 
./computedct -h
``````
to get help. To process lines in a file as inputs (one per line) to the DCT, do something like:
``````
./computedct ../input_values.txt
`````` 
This will process each line and output its DCT coefficients as though that line were a signal. One can also use stdin 
streaming, using the EOF as the termination signal, using something like a pipe, redirection, or simply by typing values 
in separated by commas on the console after running via a simple ```./computedct``` and of course using ^d as the EOF 
character to end input and print the results.

The second binary, ```dcttest```, can be run via:
``````
./dcttest
``````

It will then run and should output something like the following test output:
``````
[==========] Running 7 tests from 2 test suites.
[----------] Global test environment set-up.
[----------] 5 tests from DCTTest
[ RUN      ] DCTTest.DefaultConstructor
[       OK ] DCTTest.DefaultConstructor (0 ms)
[ RUN      ] DCTTest.ComputeVec4
[       OK ] DCTTest.ComputeVec4 (0 ms)
[ RUN      ] DCTTest.ComputeVec4TwiceAndVerifyBufferReset
[       OK ] DCTTest.ComputeVec4TwiceAndVerifyBufferReset (0 ms)
[ RUN      ] DCTTest.ComputeVec6DC
[       OK ] DCTTest.ComputeVec6DC (0 ms)
[ RUN      ] DCTTest.ComputeVec4Arbitrary
[       OK ] DCTTest.ComputeVec4Arbitrary (0 ms)
[----------] 5 tests from DCTTest (0 ms total)

[----------] 2 tests from InputStreamParserTest
[ RUN      ] InputStreamParserTest.Constructor
[       OK ] InputStreamParserTest.Constructor (0 ms)
[ RUN      ] InputStreamParserTest.ParsesInputAndComputesDCT
[       OK ] InputStreamParserTest.ParsesInputAndComputesDCT (0 ms)
[----------] 2 tests from InputStreamParserTest (0 ms total)

[----------] Global test environment tear-down
[==========] 7 tests from 2 test suites ran. (0 ms total)
[  PASSED  ] 7 tests.
``````

The tests are located in test/src and are built against the [Googletest framework](https://github.com/google/googletest "Googletest Github page").
