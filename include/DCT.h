/**
 * DCT - Discrete Cosine Transform
 */

#ifndef DEMOS_DCT_H
#define DEMOS_DCT_H

#include <memory>
#include <cmath>
#include <unordered_map>
#include <array>
#include <vector>

namespace dsp {

class DCT {
public:
    DCT() = default;

    void computeDct(std::vector<double_t> &signal, std::vector<double_t> &bufOutput,
                    double_t scale = 2.0);

private:
    std::unordered_map<size_t, std::vector<double_t>> mapCosines;

    void memoizeCosines(size_t blockSize);
};

}

#endif //DEMOS_DCT_H
