/*
 * Basic stream parser for DCT demo app
 */
#ifndef DSP_INPUTSTREAMPARSER_H
#define DSP_INPUTSTREAMPARSER_H

#include <istream>
#include <memory>
#include <vector>

namespace dsp {

class InputStreamParser {
public:
    InputStreamParser() = delete;
    InputStreamParser(std::istream &_inputStream, std::unique_ptr<DCT>& _dct); // This class will assume that this stream is thread safe

    std::vector<std::vector<double_t>> beginParsing();

private:
    std::istream& inputStream;
    std::shared_ptr<DCT> dct;
    std::vector<std::vector<double_t>> vecInputs;
};

}

#endif //DSP_INPUTSTREAMPARSER_H
