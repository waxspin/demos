/**
 * DCT Demo - dct app source
 */
#include <iostream>
#include <memory>
#include <fstream>
#include <iterator>
#include "DCT.h"
#include "InputStreamParser.h"
#include "tclap/CmdLine.h"

using namespace dsp;

int main(int argc, char **argv) {
    try {
        TCLAP::CmdLine cmd("DCT will allow the user to parse input values from a file or stdin, where each line will "
                           "count as one input vector, and it will output the corresponding one-dimensional DCT-II for"
                           " each line of input.", ' ', "0.1");
        TCLAP::UnlabeledValueArg<std::string> filename("filename", "This is a text file that contains the input data to "
                                                                   "be processed. Alternatively, one may use stdin, but "
                                                                   "one or the other must be present. As a note, stdin "
                                                                   "mode responds to EOF as expected, and will exit and "
                                                                   "print the results accordingly.",
                false, "", "filenameString");
        cmd.add(filename);
        cmd.parse(argc, argv);


        auto dct = std::make_unique<DCT>();
        std::ifstream inputFile;
        bool useFile = !filename.getValue().empty();
        auto inputParser = std::make_unique<InputStreamParser>(useFile ? inputFile : std::cin, dct);

        // Open the file if necessary
        if (useFile) {
            inputFile.open(filename.getValue());
            if (!inputFile.good()) {
                std::cerr << "Error opening file: " << filename.getValue() << std::endl;
                return EXIT_FAILURE;
            }
        }

        // Tell the parser to read from the stream
        auto outputs = inputParser->beginParsing();

        // Close the file if necessary
        if (inputFile.is_open()){
            inputFile.close();
        }

        // Lets print the results to the console
        for (const auto &transformed : outputs) {
            std::ostringstream strOut;
            std::copy(transformed.begin(), transformed.end() - 1, std::ostream_iterator<double_t>(strOut, ", "));
            strOut << transformed.back();
            std::cout << "DCT output: " << strOut.str() << std::endl;
        }

    } catch (TCLAP::ArgException &e) {
        std::cerr << "There was an error (" << e.error() << ") parsing argument: " << e.argId() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}