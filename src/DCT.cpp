/**
 * DCT - Discrete Cosine Transform
 */
#include "DCT.h"

using namespace dsp;

/**
 * This method memoizes cosine terms used in the DCT.
 *
 * @param blockSize
 */
void DCT::memoizeCosines(size_t blockSize) {
    auto iter = mapCosines.find(blockSize);
    if (iter == mapCosines.end()) {
        // We need to generate these terms
        double_t piN = M_PI / blockSize;
        auto memoization = mapCosines.insert(std::make_pair(blockSize, std::vector<double_t>(blockSize * blockSize)));
        auto itrInsert = memoization.first;
        std::vector<double_t> cosines = itrInsert->second;

        //O(N^2) but turns out this isn't all that bad, performance wise, could potentially be optimized further though...
        for (size_t k = 0; k < blockSize; k++) {
            for (size_t n = 0; n < blockSize; n++) {
                cosines.at(n + (k * blockSize)) = cos(piN * (n + 0.5) * k);
            }
        }

        itrInsert->second = cosines; // This has to be reassigned, since we are copying values
    }
}

void DCT::computeDct(std::vector<double_t> &signal, std::vector<double_t> &bufOutput,
                     double_t scale) {
    // Make sure the output buffer is empty
    bufOutput.clear();

    auto len = signal.size();
    auto iterCos = mapCosines.find(signal.size());

    //Memoize cosine terms
    if (iterCos == mapCosines.end()) {
        memoizeCosines(len);
        // reassign new mapped terms to iter
        iterCos = mapCosines.find(len);
    }

    //Get the cosine terms and compute
    auto cosines = iterCos->second;

    // This is the unoptimized O(N^2) implementation, which it turns out is still pretty fast, but if
    // further optimizing is needed, we can squeeze a bit more out through an FFT-like factorization
    for (size_t k = 0; k < len; k++) {
        double_t cur = 1.0;
        double_t prev = 0.0;
        for (size_t i = 0; i < len; i++) {
            cur = signal.at(i);
            prev = prev + (cur * cosines.at(i + (k * len)));
        }
        bufOutput.emplace_back(scale * prev);
    }
}

