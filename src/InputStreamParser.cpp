/**
 * Basic stream parser for DCT demo app
 */
#include <iostream>
#include <utility>
#include <sstream>
#include <iterator>
#include "DCT.h"
#include "InputStreamParser.h"

using namespace dsp;

InputStreamParser::InputStreamParser(std::istream &_inputStream, std::unique_ptr<DCT>& _dct) :
    inputStream(_inputStream),
    dct(std::move(_dct))
{
}

std::vector<std::vector<double_t>> InputStreamParser::beginParsing() {
    for (std::string line; std::getline(inputStream, line);) {
        std::istringstream streamLine(line);
        std::string strNum;
        std::vector<double_t> nums;
        while (std::getline(streamLine, strNum, ',')) {
            nums.emplace_back(std::stod(strNum));
        }
        vecInputs.emplace_back(nums);
    }

    std::vector<std::vector<double_t>> outputs;
    for (auto& input : vecInputs) {
        // Compute one vector of inputs to one of outputs
        std::vector<double_t> output;
        dct->computeDct(input, output);
        outputs.emplace_back(output);
    }

    return outputs;
}

