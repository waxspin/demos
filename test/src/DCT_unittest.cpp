/**
 * DCT - Discrete Cosine Transform
 */
#include <gtest/gtest.h>
#include <DCT.h>
#include "../include/test_constants.h"

using namespace dsp;
using namespace testing;

class DCTTest : public Test {
public:
    DCTTest():
        dct(nullptr)
    {
        dct = new DCT();
    }

    ~DCTTest() override {
        delete dct;
    }

protected:
    DCT *dct;

    std::vector<double_t> data_4 = {1.0, 0.0, 1.0, 0.0};
    std::vector<double_t> data_6dc = {1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
    std::vector<double_t> data_4_arbitrary = {2.0, 0.5, 0.1, 5.0};

    std::vector<double_t> out;
};

TEST_F(DCTTest, DefaultConstructor) {
    EXPECT_TRUE(dct != nullptr);
    EXPECT_EQ(sizeof(DCT), sizeof *dct);
}

TEST_F(DCTTest, ComputeVec4) {
    dct->computeDct(data_4, out);
    EXPECT_DOUBLE_EQ(4.0, out.at(0));
    EXPECT_DOUBLE_EQ(1.082392200292394, out.at(1));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(2) - 0.0));
    EXPECT_DOUBLE_EQ(2.6131259297527527, out.at(3));
}

TEST_F(DCTTest, ComputeVec4TwiceAndVerifyBufferReset) {
    dct->computeDct(data_4, out);
    EXPECT_DOUBLE_EQ(4.0, out.at(0));
    EXPECT_DOUBLE_EQ(1.082392200292394, out.at(1));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(2) - 0.0));
    EXPECT_DOUBLE_EQ(2.6131259297527527, out.at(3));

    dct->computeDct(data_4, out);
    EXPECT_DOUBLE_EQ(4.0, out.at(0));
    EXPECT_DOUBLE_EQ(1.082392200292394, out.at(1));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(2) - 0.0));
    EXPECT_DOUBLE_EQ(2.6131259297527527, out.at(3));
}

TEST_F(DCTTest, ComputeVec6DC) {
    dct->computeDct(data_6dc, out);
    EXPECT_DOUBLE_EQ(12.0, out.at(0));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(1) - 0.0));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(2) - 0.0));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(3) - 0.0));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(4) - 0.0));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(5) - 0.0));
}

TEST_F(DCTTest, ComputeVec4Arbitrary) {
    dct->computeDct(data_4_arbitrary, out);
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(0) - 15.2));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(1) + 5.24));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(2) - 9.05));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(out.at(3) + 3.04));
}