/**
 * DSP - InputStreamParser unit tests
 */
#include <gtest/gtest.h>
#include <iostream>
#include "DCT.h"
#include "InputStreamParser.h"
#include "../include/test_constants.h"

using namespace testing;
using namespace dsp;

class InputStreamParserTest : public Test {
public:
    InputStreamParserTest() {
        dct = std::make_unique<DCT>();
    }

    ~InputStreamParserTest() override {
        delete parser;
    }

protected:
    InputStreamParser *parser;
    std::unique_ptr<DCT> dct;

    void setupParser(std::istream& _stream) {
        parser = new InputStreamParser(_stream, dct);
    }

};

TEST_F(InputStreamParserTest, Constructor) {
    setupParser(std::cin);
    EXPECT_TRUE(parser != nullptr);
    EXPECT_EQ(sizeof(InputStreamParser), sizeof *parser);
}

TEST_F(InputStreamParserTest, ParsesInputAndComputesDCT) {
    std::string rawLines = "1.0, 0.0, 1.0, 0.0\n1.0, 1.0, 1.0, 1.0, 1.0, 1.0\n2.0, 0.5, 0.1, 5.0\n";
    std::istringstream issLines(rawLines);

    setupParser(issLines);
    auto output = parser->beginParsing();
    auto first = output.at(0);
    auto second = output.at(1);
    auto third = output.at(2);

    EXPECT_EQ(3, output.size());
    EXPECT_EQ(4, first.size());
    EXPECT_EQ(6, second.size());
    EXPECT_EQ(4, third.size());

    EXPECT_DOUBLE_EQ(4.0, first.at(0));
    EXPECT_DOUBLE_EQ(1.082392200292394, first.at(1));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(first.at(2) - 0.0));
    EXPECT_DOUBLE_EQ(2.6131259297527527, first.at(3));

    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(second.at(1) - 0.0));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(second.at(2) - 0.0));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(second.at(3) - 0.0));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(second.at(4) - 0.0));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(second.at(5) - 0.0));

    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(third.at(0) - 15.2));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(third.at(1) + 5.24));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(third.at(2) - 9.05));
    EXPECT_GT(TEST_THRESHOLD_FOR_EQ, fabs(third.at(3) + 3.04));
}